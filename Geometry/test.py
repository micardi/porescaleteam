import randomgeo as geo
import openfoam as of

g=geo.geom(name="test")

s=of.solver(geom=g)
s.setup(g)
